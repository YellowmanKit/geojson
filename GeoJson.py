import pandas as pd
import json

class GeoJson:

    def __init__(self, csv_file):
        self.frame = pd.read_csv('./data/' + csv_file)
        print(self.frame.shape)
        f = open('./data/mtr_location.json')
        self.coordinates = json.load(f)

    def transform(self):
        self.geo = {
          'type': 'FeatureCollection',
          'features': []
        }
        stations = {}
        for row_index in range(self.frame.shape[0]):
            #CSC_PHY_ID = self.frame.iat[row_index, 0]
            #TXN_SUBTYPE_CO = self.frame.iat[row_index, 1]
            TRAIN_ENTRY_STN = self.frame.iat[row_index, 2]
            TXN_LOC = self.frame.iat[row_index, 3]
            #ENTRY_TIME = self.frame.iat[row_index, 4]
            #EXIT_TIME = self.frame.iat[row_index, 5]

            if not str(TRAIN_ENTRY_STN) in stations:
                stations[str(TRAIN_ENTRY_STN)] = {
                    'name': self.coordinates[str(TRAIN_ENTRY_STN)]['name'],
                    'count': 1
                }
            else:
                stations[str(TRAIN_ENTRY_STN)]['count'] += 1
            if not str(TXN_LOC) in stations:
                stations[str(TXN_LOC)] = {
                    'name': self.coordinates[str(TXN_LOC)]['name'],
                    'count': 1
                }
            else:
                stations[str(TXN_LOC)]['count'] += 1

        for station in stations:
            point = self.coordinates[str(station)]['coordinates']
            feature = {
                'type': 'feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': point
                },
                'properties': stations[station]
            }
            self.geo['features'].append(feature)

    def export(self, file):
        with open('./data/' + file, 'w') as f:
            json.dump(self.geo, f, indent=4)
