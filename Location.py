import pandas as pd
import json

class Location:
    def __init__(self):
        result = {}
        geolocation = pd.read_csv('./data/mtr_geolocation.csv')
        station_number = pd.read_csv('./data/station_number.csv')
        station_number.sort_values(by=['Station number'], inplace=True)
        for row_index in range(station_number.shape[0]):
            number = station_number.iat[row_index, 1]
            name = station_number.iat[row_index, 2]
            for row_index2 in range(geolocation.shape[0]):
                name2 = geolocation.iat[row_index2, 6]
                if name2.startswith(name):
                    start = geolocation.iat[row_index2, 4]
                    end = geolocation.iat[row_index2, 5]
                    result[str(number)] = {
                        "name": name,
                        "coordinates": [start, end]
                    }

                    break
        with open('./data/mtr_location.json', 'w') as f:
            json.dump(result, f, indent=4)
